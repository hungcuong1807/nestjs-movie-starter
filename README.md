# Funny movies: Nest.js server

### Project structure
I built up project follow domain-driven pattern and repository pattern. Base on this pattern (repository => service => controller), I separated project into folders

- __configs__: folder which include application configuration
- __constants__: folder store application constants
- __database__: folder include migration files and seeder files
- __dtos__: folder include schema object
- __filters__: folder constains query filter interfaces to run query database
- __guards__: folder contains guards (use middleware)
- __models__: folder defined schema table database use knex.js
- __modules__: folder contains share module which can injected to main module
- __repositories__: folder contains repository interfaces which wrap query database
- __services__: combine repositories to get or update database
- __controllers__:

Besides, I defined test folder, which include e2e tests. 

I used postgresql and designed database.

[![share-movie-database-public.png](https://i.postimg.cc/vTCNc935/share-movie-database-public.png)](https://postimg.cc/XBc8PZMv)

### Project function

- __AUTHENTICATE__: basic authentication function (register, login, logout). When user register an account, we need to check existed email, invalid email. When user login application, we also need to check account existed system. When I implement logout function, I have to use redis to revoke access token's user

- __SHARE_MOVIE__: get movies and share movie. When users want to share a video, they have to use access token to access system. 

If have more time, I can expend your system such as authentication service we can add verify email module, forgotpassword,... With movie service, we can vote video, or react video...
### Run project
```bash init.dev.sh```
