import { configDb, configAuth } from '@configs';
import { REPOSITORIES } from '@constants';
import { AuthenticateController, MovieController } from '@controllers';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserRepository, MovieRepository } from './repositories';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from 'passports';
import { UserService, MovieService } from 'services';
import { ObjectionModule } from '@modules/objection';
import Redis from 'ioredis';

const controllers = [AuthenticateController, MovieController];
const repositories = [
  { provide: REPOSITORIES.USER_REPOSITORY, useClass: UserRepository },
  { provide: REPOSITORIES.MOVIE_REPOSITORY, useClass: MovieRepository },
];
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
      load: [configDb, configAuth],
    }),
    ObjectionModule.registerAsync({
      isGlobal: true,
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.get('db'),
      inject: [ConfigService],
    }),
    JwtModule.registerAsync({
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('auth.key.token_secret_key'),
      }),
      inject: [ConfigService],
    }),
  ],
  controllers,
  providers: [
    JwtStrategy,
    UserService,
    MovieService,
    ...repositories,
    {
      provide: 'CACHE',
      useFactory: (configService: ConfigService) => {
        const host = configService.get<string>('auth.cache.cache_host');
        const port = configService.get<string>('auth.cache.cache_port');

        const redis = new Redis({
          host,
          port: Number(port),
          lazyConnect: true,
        });
        return redis;
      },
      inject: [ConfigService],
    },
  ],
})
export class AppModule {}
