import { registerAs } from '@nestjs/config';

export const configAuth = registerAs('auth', () => ({
  auth: {
    refresh_token_lifetime: '14 days',
    access_token_lifetime: '7 days',
  },
  key: {
    token_secret_key: process.env.TOKEN_SECRET_KEY,
  },
  cache: {
    cache_host: process.env.REDIS_HOST,
    cache_port: process.env.REDIS_PORT,
  },
}));
