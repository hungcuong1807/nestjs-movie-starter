import { CreateUserDto, LoginUserDto } from '@dtos';
import { JwtAuthGuard } from '@guards';
import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  UseGuards,
  Request,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserService } from '@services';

@ApiTags('Authenticate')
@Controller('auth')
export class AuthenticateController {
  constructor(private readonly userService: UserService) {}
  @Post('signup')
  async registerWithEmail(@Body() data: CreateUserDto) {
    const user = await this.userService.registerUser(data);
    return { data: user };
  }

  @Post('signin')
  async loginWithEmail(@Body() data: LoginUserDto) {
    const user = await this.userService.loginUser(data);
    return { data: user };
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  async getProfile(@Request() req: any) {
    const userInfo = await this.userService.getUserInfo(req.user.sub);
    return { data: { id: userInfo.id, email: userInfo.email } };
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete('logout')
  async logout(@Request() req: any) {
    await this.userService.logOutUser(req.user.jti);
    return { data: true };
  }
}
