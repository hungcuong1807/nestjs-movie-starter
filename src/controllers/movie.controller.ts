/* eslint-disable prettier/prettier */
import { ShareMovieDto } from '@dtos';
import { MovieFilter } from '@filters';
import { JwtAuthGuard } from '@guards';
import {
  Controller,
  Post,
  Get,
  UseGuards,
  Request,
  Body,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { MovieService } from '@services';

@ApiTags('Movie')
@Controller('movies')
export class MovieController {
  constructor(private readonly movieService: MovieService) { }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('share')
  async shareMovie(@Request() req: any, @Body() data: ShareMovieDto) {
    return {data: await this.movieService.shareMovieByUser({
      ...data,
      share_user_id: req.user.sub,
    })};
  }

  @Get('')
  async getListMovies(@Query() query: MovieFilter) {
    const movies = await this.movieService.getListMovie(query);
    return {data: movies}
  }
}
