import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('movies', function (table) {
    table.uuid('id').primary();
    table.text('title').nullable();
    table.text('description').nullable();
    table.text('video_url').notNullable();
    table.uuid('share_user_id').references('id').inTable('users');

    table.timestamp('created_at').defaultTo(knex.fn.now());
    table.timestamp('updated_at').defaultTo(knex.fn.now());
    table.timestamp('deleted_at').defaultTo(null);

    table.unique(['video_url', 'share_user_id']);
  });
}

export async function down(knex: Knex): Promise<void> {}
