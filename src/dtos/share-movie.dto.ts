import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString, IsUUID, IsUrl } from 'class-validator';

export class ShareMovieDto {
  @ApiPropertyOptional({ example: 'title' })
  @IsOptional()
  @IsString()
  title?: string;

  @ApiPropertyOptional({ example: 'description' })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({ example: 'http://youtube.com' })
  @IsOptional()
  @IsUrl()
  video_url: string;
}

export class ShareMovieExtendDto {
  @IsUUID(4)
  share_user_id: string;
}
