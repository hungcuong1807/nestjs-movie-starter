import { BaseFilter } from '@modules/objection';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString, IsUUID } from 'class-validator';

export class MovieFilter extends BaseFilter {
  @ApiPropertyOptional({ example: 'search title' })
  @IsOptional()
  @IsString()
  search_text?: string;

  @IsOptional()
  @IsUUID(4, { each: true })
  share_user_ids?: string[];
}
