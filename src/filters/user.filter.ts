import { BaseFilter } from '@modules/objection';
import { IsEmail, IsOptional, IsUUID } from 'class-validator';

export class UserFilter extends BaseFilter {
  @IsOptional()
  @IsUUID(4, { each: true })
  ids?: string[];

  @IsOptional()
  @IsEmail()
  email?: string;
}
