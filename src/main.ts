import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import morgan from 'morgan';
import { checkDatabaseConnection, ConfigAppService } from '@configs';
import { Logger, ValidationPipe, VersioningType } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const DEFAULT_API_VERSION = '1';

async function bootstrap() {
  await checkDatabaseConnection();
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const options = new DocumentBuilder()
    .setTitle('API docs')
    .setVersion(DEFAULT_API_VERSION)
    .addBearerAuth()
    .build();

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  app.enableVersioning({
    defaultVersion: DEFAULT_API_VERSION,
    type: VersioningType.URI,
  });

  app.enableCors();

  app.use(morgan('tiny'));
  const port = new ConfigAppService().get('appPort');
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}/v${DEFAULT_API_VERSION}`,
  );
}
bootstrap();
