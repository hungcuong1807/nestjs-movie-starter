import { BaseModel } from '@modules/objection';
import { UserModel } from '@models';
import { JSONSchema } from 'objection';
class MovieModel extends BaseModel {
  static tableName = 'movies';
  static connection = 'postgres';
  static useUUID = true;
  id: string;
  title: string | null;
  description: string | null;
  video_url: string;
  share_user_id: string;
  created_at?: Date | null;
  updated_at?: Date | null;
  deleted_at?: Date | null;
  user: UserModel;

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'string' },
      title: { type: 'string', nullable: true },
      description: { type: 'string', nullable: true },
      video_url: { type: 'string' },
      share_user_id: { type: 'string' },
      created_at: { type: ['string', 'null'], format: 'date-time' },
      updated_at: { type: ['string', 'null'], format: 'date-time' },
      deleted_at: { type: ['string', 'null'], format: 'date-time' },
    },
  };
}
export default MovieModel;
MovieModel.relationMappings = {
  user: {
    relation: BaseModel.BelongsToOneRelation,
    modelClass: () => UserModel,
    join: {
      from: 'movies.share_user_id',
      to: 'users.id',
    },
  },
};
