import { BaseModel } from '@modules/objection';
import { MovieModel } from '.';
class UserModel extends BaseModel {
  static tableName = 'users';
  static connection = 'postgres';
  static useUUID = true;

  id: string;
  email: string;
  password: string;
  created_at?: Date | null;
  updated_at?: Date | null;
  deleted_at?: Date | null;

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'string' },
      email: { type: 'string' },
      password: { type: 'string' },
      created_at: { type: ['string', 'null'], format: 'date-time' },
      updated_at: { type: ['string', 'null'], format: 'date-time' },
      deleted_at: { type: ['string', 'null'], format: 'date-time' },
    },
  };
}
export default UserModel;
UserModel.relationMappings = {
  movies: {
    relation: BaseModel.HasManyRelation,
    modelClass: () => MovieModel,
    join: {
      from: 'users.id',
      to: 'movies.share_user_id',
    },
  },
};
