import { MovieFilter } from '@filters';
import { MovieModel } from '@models';
import { IRepository } from '@modules/objection';

export interface IMovieRepository extends IRepository<MovieModel> {
  list(
    filter?: MovieFilter,
  ): Promise<{ items: MovieModel[]; pagination: Record<string, any> }>;
}
