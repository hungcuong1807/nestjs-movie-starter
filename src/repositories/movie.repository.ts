import { MovieModel } from '@models/index';
import { InjectModel, Repository } from '@modules/objection';
import { Injectable } from '@nestjs/common';
import { IMovieRepository } from './interfaces';
import { MovieFilter } from '@filters';
import { AnyQueryBuilder, raw } from 'objection';

@Injectable()
export class MovieRepository
  extends Repository<MovieModel>
  implements IMovieRepository
{
  @InjectModel(MovieModel)
  model: MovieModel;

  static get tableName() {
    return MovieModel.tableName;
  }

  static queryFilter(
    query: AnyQueryBuilder,
    filter: MovieFilter,
  ): AnyQueryBuilder {
    if (filter.share_user_ids) {
      query = query.whereIn(
        `${this.tableName}.share_user_id`,
        filter.share_user_ids,
      );
    }
    if (filter.search_text) {
      query = query.where(
        raw(`lower(${this.tableName}.title)`),
        'like',
        `%${filter?.search_text}%`,
      );
    }
    return query;
  }

  async list(
    filter?: MovieFilter,
  ): Promise<{ items: MovieModel[]; pagination: Record<string, any> }> {
    const filterPagination = { ...filter };
    delete filterPagination.page;
    delete filterPagination.limit;
    let query = MovieRepository.queryFilter(this.query(), filter);
    query = MovieRepository.baseQueryFilter(query, filter);
    return super.paginate(query, filter?.page, filter?.limit);
  }
}
