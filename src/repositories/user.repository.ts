import { UserModel } from '@models/index';
import { InjectModel, Repository } from '@modules/objection';
import { Injectable } from '@nestjs/common';
import { IUserRepository } from './interfaces';
import { UserFilter } from '@filters';
import { AnyQueryBuilder } from 'objection';

@Injectable()
export class UserRepository
  extends Repository<UserModel>
  implements IUserRepository
{
  @InjectModel(UserModel)
  model: UserModel;

  static get tableName() {
    return UserModel.tableName;
  }

  static queryFilter(
    query: AnyQueryBuilder,
    filter: UserFilter,
  ): AnyQueryBuilder {
    if (filter.ids) {
      query = query.whereIn('id', filter.ids);
    }
    if (filter.email) {
      query = query.where('email', filter.email);
    }
    return query;
  }

  async list(filter?: UserFilter): Promise<UserModel[]> {
    let query = UserRepository.queryFilter(this.query(), filter);
    query = UserRepository.baseQueryFilter(query, filter);
    return query;
  }
}
