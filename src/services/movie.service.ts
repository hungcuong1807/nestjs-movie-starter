import { REPOSITORIES } from '@constants';
import { ShareMovieExtendDto } from '@dtos';
import { MovieFilter } from '@filters';
import { Inject, Injectable } from '@nestjs/common';
import { IMovieRepository, IUserRepository } from '@repositories/interfaces';

@Injectable()
export class MovieService {
  @Inject(REPOSITORIES.MOVIE_REPOSITORY)
  private movieRepository: IMovieRepository;

  @Inject(REPOSITORIES.USER_REPOSITORY)
  private userRepository: IUserRepository;

  async shareMovieByUser(movie: ShareMovieExtendDto) {
    const movieInfo = await this.movieRepository.create(movie);
    return movieInfo;
  }

  async getListMovie(filter?: MovieFilter) {
    const movieList = await this.movieRepository.list(filter);
    const userIds = new Set([
      ...movieList['items'].map((item) => item.share_user_id),
    ]);
    const userInfos = await this.userRepository.list({
      ids: [...userIds],
    });

    const userObject: { [key: string]: string } = {};
    userInfos.forEach((item) => {
      userObject[item.id] = item.email;
    });

    const items = movieList['items'].map((item) => ({
      ...item,
      shared_user_email: userObject[item.share_user_id],
    }));
    return { ...movieList, items };
  }
}
