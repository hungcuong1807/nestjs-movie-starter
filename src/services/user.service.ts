import { REPOSITORIES } from '@constants';
import { CreateUserDto, LoginUserDto } from '@dtos';
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { IUserRepository } from '@repositories/interfaces';
import { compare, getHash } from '@utils';
import Redis from 'ioredis';
import ms from 'ms';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class UserService {
  constructor(
    @Inject(REPOSITORIES.USER_REPOSITORY)
    private userRepository: IUserRepository,

    @Inject('CACHE')
    private cache: Redis,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async registerUser(user: CreateUserDto) {
    const password = getHash(user.password);
    const userExist = await this.userRepository
      .query()
      .where('email', user.email)
      .first();
    if (userExist) {
      throw new BadRequestException('The account existed');
    }
    const userInfo = await this.userRepository.create({
      ...user,
      password: password,
    });
    const jti = uuidv4();
    const expired_at = Math.floor(
      (Date.now() +
        ms(this.configService.get<string>('auth.auth.access_token_lifetime'))) /
        1000,
    );
    await this.cache.set(`access_token:${jti}`, userInfo.id);
    await this.cache.expireat(`access_token:${jti}`, +expired_at);
    return {
      access_token: await this.jwtService.signAsync(
        {
          id: userInfo.id,
          jti,
        },
        {
          expiresIn: ms(
            this.configService.get<string>('auth.auth.access_token_lifetime'),
          ),
        },
      ),
      expires_in: ms(
        this.configService.get<string>('auth.auth.access_token_lifetime'),
      ),
      token_type: 'Bearer',
      user: {
        id: userInfo.id,
        email: userInfo.email,
      },
    };
  }

  async loginUser(user: LoginUserDto) {
    const userInfo = await this.userRepository.list({ email: user.email });
    if (!userInfo.length) {
      throw new BadRequestException('Not exist email');
    }
    if (!compare(user.password, userInfo[0].password)) {
      throw new BadRequestException('Incorrect password');
    }
    const jti = uuidv4();
    const expired_at = Math.floor(
      (Date.now() +
        ms(this.configService.get<string>('auth.auth.access_token_lifetime'))) /
        1000,
    );
    await this.cache.set(`access_token:${jti}`, userInfo[0].id);
    await this.cache.expireat(`access_token:${jti}`, +expired_at);
    return {
      access_token: await this.jwtService.signAsync(
        {
          id: userInfo[0].id,
          jti,
        },
        {
          expiresIn: ms(
            this.configService.get<string>('auth.auth.access_token_lifetime'),
          ),
        },
      ),
      expires_in: ms(
        this.configService.get<string>('auth.auth.access_token_lifetime'),
      ),
      token_type: 'Bearer',
      user: {
        id: userInfo[0].id,
        email: userInfo[0].email,
      },
    };
  }

  async getUserInfo(id: string) {
    return await this.userRepository.findById(id);
  }
  async logOutUser(jti: string) {
    return await this.cache.del(`access_token:${jti}`);
  }
}
