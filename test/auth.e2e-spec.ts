/* eslint-disable prettier/prettier */
import request from 'supertest';
describe('Testing auth api', () => {
  const API_HOST = `http://localhost:5000/api/v1`;
  const email = `user${Date.now()}@gmail.com`;
  describe('register account', () => {
    it('[Success case] Register an ancount', async () => {
      const response = await request(API_HOST)
        .post('/auth/signup')
        .send({
          email: email,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.data).toHaveProperty('user');
      expect(response.body.data.user).toHaveProperty('email');
      expect(response.body.data.user.email).toEqual(email);
    });

    it('[Failed case] Register an account invalid email', async () => {
      const email2 = `${Date.now}@`;
      const response = await request(API_HOST)
        .post('/auth/signup')
        .send({
          email: email2,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.statusCode).toEqual(400);
    });

    it('[Failed case] Register an account exsited email', async () => {
      const response = await request(API_HOST)
        .post('/auth/signup')
        .send({
          email: email,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.statusCode).toEqual(400);
    });
  });

  describe('Testing login api', () => {
    it('[Success case] Login an ancount', async () => {
      const response = await request(API_HOST)
        .post('/auth/signin')
        .send({
          email: email,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.data).toHaveProperty('user');
      expect(response.body.data.user).toHaveProperty('email');
      expect(response.body.data.user.email).toEqual(email);
    });

    it('[Failed case] login with not existed email', async () => {
      const email2 = `user-${Date.now()}@gmail.com`;
      const response = await request(API_HOST)
        .post('/auth/signin')
        .send({
          email: email2,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.statusCode).toEqual(400);
    });

    it('[Failed case] login with not invalid email', async () => {
      const email2 = `user-${Date.now()}@`;
      const response = await request(API_HOST)
        .post('/auth/signin')
        .send({
          email: email2,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.statusCode).toEqual(400);
    });
  });

  describe('Testing logout', () => {
    it('Logout function', async () => {
      
     // logout => get profile => unauthorization
      const responseLogin = await request(API_HOST)
        .post('/auth/signin')
        .send({
          email: email,
          password: 'Test1234@',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      const accessToken = responseLogin.body.data['access_token'];
      await request(API_HOST)
        .delete('/auth/logout')
        .set('Authorization', `Bearer ${accessToken}`);

      const responseProfile = await request(API_HOST).get('/auth/profile');
      expect(responseProfile.body.statusCode).toEqual(401);
    });
  });
});
