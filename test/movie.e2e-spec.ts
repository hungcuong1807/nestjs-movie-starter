/* eslint-disable prettier/prettier */
import request from 'supertest';
describe('Testing get movie share api', () => {
  const API_HOST = `http://localhost:5000/api/v1`;
  let accessToken = '';
  const email = `user-${Date.now()}@gmail.com`;
  beforeAll(async () => {
    const register = await request(API_HOST).post('/auth/signup').send({
      email: email,
      password: 'Test1234@',
    });
    accessToken = register.body.data.access_token;
  });
  describe('Get movie list', () => {
    it('Get movies', async () => {
      const response = await request(API_HOST)
        .get('/movies')
        .query({ page: 1, limit: 20 })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      expect(response.body.data).toHaveProperty('items');
    });

    it('[FAILED CASE] share movie is invalid url', async () => {
      const response = await request(API_HOST)
        .post('/movies/share')
        .send({
          video_url: 'asdasdasdasds',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`);
      expect(response.body.statusCode).toEqual(400);
    });

    it('[Success CASE] share movie is invalid url', async () => {
      const response = await request(API_HOST)
        .post('/movies/share')
        .send({
          video_url:
            'https://www.youtube.com/watch?v=4lxiWubYf2w&list=RD4lxiWubYf2w&index=2',
          title: 'Music',
          description: 'Test',
        })
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`);

      expect(response.body.data).toHaveProperty('id');
    });
  });
});
